package com.example.webclient.dto;

public class UpdateEmptySeatNoInputDTO {
    private Integer busID;
    private Integer seatLeft;

    public Integer getSeatLeft() {
        return seatLeft;
    }

    public void setSeatLeft(Integer seatLeft) {
        this.seatLeft = seatLeft;
    }

    public Integer getBusID() {
        return busID;
    }

    public void setBusID(Integer busID) {
        this.busID = busID;
    }

}

