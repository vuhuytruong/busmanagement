package com.example.webclient.dto;

public class UpdateEmptySeatNoResponseDTO {
    private Integer seatNo;
    private Integer busID;

    public Integer getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(Integer seatNo) {
        this.seatNo = seatNo;
    }

    public Integer getBusID() {
        return busID;
    }

    public void setBusID(Integer busID) {
        this.busID = busID;
    }
}
