package com.example.webclient.dto;

import java.util.List;

public class BillDTO {
    private Float totalPayment;
    private Integer numberOfCustomer;

    private List<TicketResponseDTO> ticketResponseDTOList;

    public Float getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(Float totalPayment) {
        this.totalPayment = totalPayment;
    }

    public Integer getNumberOfCustomer() {
        return numberOfCustomer;
    }

    public void setNumberOfCustomer(Integer numberOfCustomer) {
        this.numberOfCustomer = numberOfCustomer;
    }

    public List<TicketResponseDTO> getTicketResponseDTOList() {
        return ticketResponseDTOList;
    }

    public void setTicketResponseDTOList(List<TicketResponseDTO> ticketResponseDTOList) {
        this.ticketResponseDTOList = ticketResponseDTOList;
    }
}
