package com.example.webclient.dto;


import java.util.Date;

public class BusScheduleInputRequestDTO {

    private Long routeId;

    private Date departure;

    private Date arrival;

    private Float fare;
    private Date pickUpTime;

    private String pickUpPoint;

    private Date dropOffTime;

    private String dropOffPoint;

    private Integer emptySeatLeft;

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Float getFare() {
        return fare;
    }

    public void setFare(Float fare) {
        this.fare = fare;
    }

    public Date getPickUpTime() {
        return pickUpTime;
    }

    public void setPickUpTime(Date pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public String getPickUpPoint() {
        return pickUpPoint;
    }

    public void setPickUpPoint(String pickUpPoint) {
        this.pickUpPoint = pickUpPoint;
    }

    public Date getDropOffTime() {
        return dropOffTime;
    }

    public void setDropOffTime(Date dropOffTime) {
        this.dropOffTime = dropOffTime;
    }

    public String getDropOffPoint() {
        return dropOffPoint;
    }

    public void setDropOffPoint(String dropOffPoint) {
        this.dropOffPoint = dropOffPoint;
    }

    public Integer getEmptySeatLeft() {
        return emptySeatLeft;
    }

    public void setEmptySeatLeft(Integer emptySeatLeft) {
        this.emptySeatLeft = emptySeatLeft;
    }
}
