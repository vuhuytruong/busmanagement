package com.example.webclient.dto;


import java.util.Date;

public class TicketDTO {
    private Long ticketId;

    private Long customerId;

    private Long routeId;

    private Long scheduleId;

    private Long busId;
    private Integer status;

    private Integer deleted;

    private Date updatedAt;

    private Date createdAt;

}
