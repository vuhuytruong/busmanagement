package com.example.webclient.service;

import com.example.webclient.dto.*;
import com.example.webclient.exception.BookingException;

import java.util.List;
public interface BookingService {
    List<BusDTO> getBusListToBook(GetBusListRequestDTO requestDTO) throws BookingException;

    BillDTO book(BookRequestDTO requestDTO) throws BookingException;

}
