package com.example.webclient.service.Impl;

import com.example.webclient.dto.*;
import com.example.webclient.exception.BookingException;
import com.example.webclient.model.*;
import com.example.webclient.model.Bus;
import com.example.webclient.model.BusSchedule;
import com.example.webclient.repository.*;
import com.example.webclient.repository.BusRepository;
import com.example.webclient.service.BookingService;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class BookingServiceImpl implements BookingService {
    private final RouteRepository routeRepository;
    private final BusScheduleRepository busScheduleRepository;
    private final BusRepository busRepository;
    private final TicketRepository ticketRepository;
    private final CustomerRepository customerRepository;

    public BookingServiceImpl(
            RouteRepository routeRepository,
            BusScheduleRepository busScheduleRepository,
            BusRepository busRepository,
            TicketRepository ticketRepository, CustomerRepository customerRepository) {
        this.routeRepository = routeRepository;
        this.busScheduleRepository = busScheduleRepository;
        this.busRepository = busRepository;
        this.ticketRepository = ticketRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public List<BusDTO> getBusListToBook(GetBusListRequestDTO requestDTO) throws BookingException {
        if (requestDTO.getStartingPoint() == null || requestDTO.getEndingPoint() == null
                || requestDTO.getDepartureFrom() == null || requestDTO.getDepartureTo() == null) {
            throw new BookingException("Vui long nhap du du lieu");
        }

        Optional<Route> optionalRoute = routeRepository
                .findByRouteFromAndRouteTo(requestDTO.getStartingPoint(), requestDTO.getEndingPoint());

        if (optionalRoute.isEmpty()) {
            throw new BookingException("Khong tim thay chuyen phu hop");
        }

        Route routeEntity = optionalRoute.get();

        Date departFrom = convertStringToDate(requestDTO.getDepartureFrom() + "T00:00:00Z");
        Date departTo = convertStringToDate(requestDTO.getDepartureTo() + "T23:59:59Z");

        List<BusSchedule> busScheduleList = busScheduleRepository
                .findByRouteIdAndAndDepartureBetween(routeEntity.getRouteId(),
                        departFrom, departTo);

        if (busScheduleList.isEmpty()) {
            throw new BookingException("Khong tim thay chuyen phu hop");
        }

        List<Long> scheduId = busScheduleList.stream().map(BusSchedule::getScheduleId).toList();


        if(scheduId.isEmpty()){
            return Collections.emptyList();
        }
        List<Bus> busDTOList  = busRepository.findByScheduleId(scheduId);

        return busDTOList.stream().map(x -> {
            BusDTO busDTO = new BusDTO();
            busDTO.setBusId(x.getBusId());
            busDTO.setScheduleId(x.getScheduleId());
            busDTO.setLicenseNumber(x.getLicenseNumber());
            busDTO.setUrlImage(x.getUrlImage());
            busDTO.setSeatTotal(x.getSeatTotal());
            busDTO.setDeleted(x.getDeleted());
            busDTO.setCreatedAt(x.getCreatedAt().toString());
            busDTO.setUpdatedAt(x.getUpdatedAt()==null?null:x.getUpdatedAt().toString());
                    return busDTO;
                }).toList();
    }

    @Override
    public BillDTO book(BookRequestDTO requestDTO) throws BookingException {
       Optional<Bus> busOptional = busRepository.findById(requestDTO.getBusID());

        if (busOptional.isEmpty()) {
            throw new BookingException("Vui long chon xe");
        }

        Bus busDTOEntity = busOptional.get();

        Optional<BusSchedule> busScheduleOptional = busScheduleRepository.findById(busDTOEntity.getScheduleId());

        if (busScheduleOptional.isEmpty()) {
            throw new BookingException("Khong tim thay lich trinh phu hop");
        }

        BusSchedule busScheduleEntity = busScheduleOptional.get();

        Optional<Route> routeOptional = routeRepository.findById(busScheduleEntity.getRouteId());

        if (routeOptional.isEmpty()) {
            throw new BookingException("Khong tim thay tuyen phu hop");
        }

        Route routeEntity = routeOptional.get();

        Customer customer = new Customer();
        customer.setBusId(busDTOEntity.getBusId());
        customer.setCustomerName(requestDTO.getCustomerName());
        customer.setCustomerPhoneNumber(requestDTO.getCustomerPhoneNumber());
        customer.setCustomerEmail(requestDTO.getCustomerEmail());
        customer.setCreatedAt(Date.from(Instant.now()));
        customer.setDeleted(0);
        customer = customerRepository.save(customer);

        List<Ticket> ticketList = new ArrayList<>();

        for (int i = 0; i < requestDTO.getCustomerNumber(); i++ ){
            Ticket ticket = new Ticket();
            ticket.setCustomerId(customer.getCustomerId());
            ticket.setScheduleId(busDTOEntity.getScheduleId());
            ticket.setRouteId(busScheduleEntity.getRouteId());
            ticket.setDeleted(0);
            ticket.setCreatedAt(Date.from(Instant.now()));
            ticketList.add(ticket);
        }
        ticketList =  ticketRepository.saveAll(ticketList);

        String routeName = routeEntity.getRouteFrom() + " - " + routeEntity.getRouteTo();


        List<TicketResponseDTO> responseDTOList = new ArrayList<>();
        ticketList.forEach(x ->{
            TicketResponseDTO responseDTO = new TicketResponseDTO();
            responseDTO.setTicketNo(x.getTicketId());
            responseDTO.setRouteName(routeName);
            responseDTO.setLicenseNumber(busDTOEntity.getLicenseNumber());
            responseDTO.setDeparture(busScheduleEntity.getDeparture().toString());
            responseDTO.setFare(busScheduleEntity.getFare());
            responseDTO.setCreatedAt(Instant.now().toString());
            responseDTOList.add(responseDTO);
        });


        BillDTO billDTO = new BillDTO();
        billDTO.setTotalPayment(busScheduleEntity.getFare() * ticketList.size());
        billDTO.setNumberOfCustomer(ticketList.size());
        billDTO.setTicketResponseDTOList(responseDTOList);
        return billDTO;
    }

    private Date convertStringToDate(String input) {
        Instant instant = Instant.parse(input).minus(1,ChronoUnit.DAYS);
        ZoneId zone = ZoneId.of("Asia/Ho_Chi_Minh");
        return Date.from(instant.atZone(zone).toInstant());
    }

}
