package com.example.webclient.service.backoffice;

import com.example.webclient.dto.CustomerDTO;
import com.example.webclient.exception.CustomerException;

import java.util.List;

public interface CustomerService {

    CustomerDTO getCustomerDetail(Long id) throws CustomerException;
    List<CustomerDTO> getAllCustomer();

}
