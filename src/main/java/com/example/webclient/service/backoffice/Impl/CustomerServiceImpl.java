package com.example.webclient.service.backoffice.Impl;

import com.example.webclient.dto.CustomerDTO;
import com.example.webclient.exception.CustomerException;
import com.example.webclient.model.Customer;
import com.example.webclient.repository.CustomerRepository;
import com.example.webclient.service.backoffice.CustomerService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service

public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public CustomerDTO getCustomerDetail(Long id) throws CustomerException {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if(customerOptional.isEmpty()){
            throw new CustomerException("Khong tim thay khach hang");
        }
        Customer customerEntity = customerOptional.get();
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCustomerId(customerEntity.getCustomerId());
        customerDTO.setBusId(customerEntity.getBusId());
        customerDTO.setCustomerName(customerEntity.getCustomerName());
        customerDTO.setCustomerPhoneNumber(customerEntity.getCustomerPhoneNumber());
        customerDTO.setCustomerEmail(customerEntity.getCustomerEmail());
        customerDTO.setDeleted(customerEntity.getDeleted());
        customerDTO.setCreatedAt(customerEntity.getCreatedAt().toString());
        return customerDTO;
    }


    @Override
    public List<CustomerDTO> getAllCustomer() {
        List<Customer> customerList = customerRepository.getAll();
        if(customerList.isEmpty()){
            return null;
        }
        return customerList.stream().map(x ->{
            CustomerDTO customerDTO = new CustomerDTO();
            customerDTO.setBusId(x.getBusId());
            customerDTO.setCustomerId(x.getCustomerId());
            customerDTO.setCustomerName(x.getCustomerName());
            customerDTO.setCustomerPhoneNumber(x.getCustomerPhoneNumber());
            customerDTO.setCustomerEmail(x.getCustomerEmail());
            customerDTO.setCreatedAt(x.getCreatedAt().toString());
            customerDTO.setDeleted(x.getDeleted());
            customerDTO.setUpdatedAt(x.getUpdatedAt().toString());
            return customerDTO;
        }).toList();
    }

}
