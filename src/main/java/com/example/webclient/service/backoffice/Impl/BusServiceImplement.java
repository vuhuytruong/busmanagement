package com.example.webclient.service.backoffice.Impl;

import com.example.webclient.dto.BusDTO;
import com.example.webclient.dto.BusInputRequestDTO;
import com.example.webclient.exception.BusException;
import com.example.webclient.model.Bus;
import com.example.webclient.repository.BusRepository;
import com.example.webclient.service.backoffice.BusService;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BusServiceImplement implements BusService {
    private final BusRepository busRepository;

    public BusServiceImplement(BusRepository busRepository) {
        this.busRepository = busRepository;
    }

    @Override
    public BusDTO getDetail(Long id) throws BusException {
        Optional<Bus> busOptional = busRepository.findById(id);
        if (busOptional.isEmpty()) {
            throw new BusException("Không tìm thấy xe");
        }
        Bus busEntity = busOptional.get();
        BusDTO busDTO = new BusDTO();
        busDTO.setBusId(busEntity.getBusId());
        busDTO.setScheduleId(busEntity.getScheduleId());
        busDTO.setLicenseNumber(busEntity.getLicenseNumber());
        busDTO.setSeatTotal(busEntity.getSeatTotal());
        busDTO.setUrlImage(busEntity.getUrlImage());
        busDTO.setDeleted(busEntity.getDeleted());
        busDTO.setUpdatedAt(busEntity.getUpdatedAt()==null?null:
                busEntity.getUpdatedAt().toString());
        busDTO.setCreatedAt(busEntity.getCreatedAt().toString());
        return busDTO ;
    }

    @Override
    public BusDTO addBus(BusInputRequestDTO busInputRequestDTO) throws BusException {
    if(busInputRequestDTO.getLicenseNumber() == null || busInputRequestDTO.getSeatTotal() == null
    ||busInputRequestDTO.getScheduleId()==null){
        throw new BusException("Vui long nhap du thong tin");
    }
    Bus busEntity = new Bus();
    busEntity.setLicenseNumber(busInputRequestDTO.getLicenseNumber());
    busEntity.setUrlImage(busInputRequestDTO.getUrlImage());
    busEntity.setSeatTotal(busInputRequestDTO.getSeatTotal());
    busEntity.setCreatedAt(Date.from(Instant.now()));
    busEntity.setDeleted(0);
    busEntity.setScheduleId(busInputRequestDTO.getScheduleId());
    busEntity =busRepository.save(busEntity);
        BusDTO busDTO = new BusDTO();
        busDTO.setLicenseNumber(busEntity.getLicenseNumber());
        busDTO.setSeatTotal(busEntity.getSeatTotal());
        busDTO.setUrlImage(busEntity.getUrlImage());
        busDTO.setCreatedAt(busEntity.getCreatedAt().toString());
        busDTO.setDeleted(busEntity.getDeleted());
        busDTO.setBusId(busEntity.getBusId());
        busDTO.setScheduleId(busEntity.getScheduleId());
        return busDTO ;
    }

    @Override
    public BusDTO updatedBus(Long id, BusInputRequestDTO bus) throws BusException {
        Optional<Bus> busOptional = busRepository.findById(id);
        if(busOptional.isEmpty()){
            throw new BusException("Khong tim thay xe");
        }
        Bus busEntity = busOptional.get();
        busEntity.setScheduleId(bus.getScheduleId());
        busEntity.setLicenseNumber(bus.getLicenseNumber());
        busEntity.setSeatTotal(bus.getSeatTotal());
        busEntity.setUrlImage(bus.getUrlImage());
        busEntity.setDeleted(0);
        busEntity.setUpdatedAt(Date.from(Instant.now()));
        busEntity = busRepository.save(busEntity);
        BusDTO busDTO = new BusDTO();
        busDTO.setBusId(busEntity.getBusId());
        busDTO.setScheduleId(busEntity.getScheduleId());
        busDTO.setLicenseNumber(busEntity.getLicenseNumber());
        busDTO.setSeatTotal(busEntity.getSeatTotal());
        busDTO.setUrlImage(busEntity.getUrlImage());
        busDTO.setDeleted(busEntity.getDeleted());
        busDTO.setUpdatedAt(busEntity.getUpdatedAt()==null?null:
                busEntity.getUpdatedAt().toString());
        busDTO.setCreatedAt(busEntity.getCreatedAt().toString());
        return busDTO ;
    }


    @Override
    public List<BusDTO> getAllBus() {
        List<Bus> busList = busRepository.getAll();
        return busList.stream().map(x ->{
            BusDTO busDTO = new BusDTO();
            busDTO.setBusId(x.getBusId());
            busDTO.setScheduleId(x.getScheduleId());
            busDTO.setLicenseNumber(x.getLicenseNumber());
            busDTO.setUrlImage(x.getUrlImage());
            busDTO.setSeatTotal(x.getSeatTotal());
            busDTO.setDeleted(x.getDeleted());
            busDTO.setCreatedAt(x.getCreatedAt()==null?null:x.getCreatedAt().toString());
            busDTO.setUpdatedAt(x.getUpdatedAt()==null?null:x.getUpdatedAt().toString());
            return busDTO;
        }).toList();

    }

    @Override
    public void delete(Long id) throws BusException {
        Optional<Bus> busOptional = busRepository.findById(id);
        if(busOptional.isEmpty()){
            throw new BusException("Khong tim thay xe");
        }
        Bus busDTOEntity = busOptional.get();
        busDTOEntity.setDeleted(1);
        busRepository.save(busDTOEntity);

    }
}
