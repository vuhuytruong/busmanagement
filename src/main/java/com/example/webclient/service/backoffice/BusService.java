package com.example.webclient.service.backoffice;

import com.example.webclient.dto.BusDTO;
import com.example.webclient.dto.BusInputRequestDTO;
import com.example.webclient.exception.BusException;

import java.util.List;

public interface BusService {
    BusDTO getDetail(Long id) throws BusException;

    BusDTO addBus(BusInputRequestDTO busDTO) throws BusException;
    BusDTO updatedBus(Long id, BusInputRequestDTO busDTO) throws BusException;
    List<BusDTO> getAllBus();
    void delete(Long id) throws BusException;

}
