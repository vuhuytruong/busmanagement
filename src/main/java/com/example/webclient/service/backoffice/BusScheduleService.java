package com.example.webclient.service.backoffice;

import com.example.webclient.dto.BusScheduleDTO;
import com.example.webclient.dto.BusScheduleInputRequestDTO;
import com.example.webclient.exception.BusScheduleException;
import com.example.webclient.model.BusSchedule;

import java.util.List;

public interface BusScheduleService {
     //Ham them lich trinh
     BusScheduleDTO addBusSchedule(BusScheduleInputRequestDTO busScheduleInputRequestDTO) throws BusScheduleException;
     //Ham chinh sua thong tin lich tinh
     BusScheduleDTO updateBusSchedule(Long id, BusScheduleInputRequestDTO busScheduleInputRequestDTO) throws BusScheduleException;
    BusScheduleDTO getBusScheduleDetail(Long id) throws BusScheduleException;
     //ham lay ra danh sach lich trinh
     List<BusScheduleDTO> getAllBusSchedule();
     void delete(Long id) throws BusScheduleException;
}
