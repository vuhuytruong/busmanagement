package com.example.webclient.service.backoffice.Impl;

import com.example.webclient.dto.BusScheduleDTO;
import com.example.webclient.dto.BusScheduleInputRequestDTO;
import com.example.webclient.exception.BusScheduleException;
import com.example.webclient.model.BusSchedule;
import com.example.webclient.repository.BusScheduleRepository;
import com.example.webclient.service.backoffice.BusScheduleService;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BusScheduleServiceImpl implements BusScheduleService {
    private final BusScheduleRepository busScheduleRepository;

    public BusScheduleServiceImpl(BusScheduleRepository busScheduleRepository) {
        this.busScheduleRepository = busScheduleRepository;
    }


    @Override
    public BusScheduleDTO addBusSchedule(BusScheduleInputRequestDTO busScheduleInputRequestDTO) throws BusScheduleException {
        if (busScheduleInputRequestDTO.getPickUpPoint() == null
                || busScheduleInputRequestDTO.getPickUpTime() == null
                || busScheduleInputRequestDTO.getDropOffTime() == null
                || busScheduleInputRequestDTO.getDropOffPoint() == null
                || busScheduleInputRequestDTO.getArrival() == null
                || busScheduleInputRequestDTO.getDeparture() == null
                || busScheduleInputRequestDTO.getEmptySeatLeft() == null
                || busScheduleInputRequestDTO.getFare() == null
                || busScheduleInputRequestDTO.getRouteId() == null){
            throw new BusScheduleException("Vui long nhap du thong tin");
        }
        Instant departureAfterMinus = busScheduleInputRequestDTO
                .getDeparture().toInstant().minus(7, ChronoUnit.HOURS);

        Instant arrivalAfterMinus = busScheduleInputRequestDTO.
                getArrival().toInstant().minus(7,ChronoUnit.HOURS);
        Instant pickUpTimeAfterMinus = busScheduleInputRequestDTO.
                getPickUpTime().toInstant().minus(7,ChronoUnit.HOURS);
        Instant dropOffTimeAfterMinus = busScheduleInputRequestDTO.
                getDropOffTime().toInstant().minus(7,ChronoUnit.HOURS);
        BusSchedule busScheduleEntity = new BusSchedule();
        busScheduleEntity.setRouteId(busScheduleInputRequestDTO.getRouteId());
        busScheduleEntity.setArrival(Date.from(arrivalAfterMinus));
        busScheduleEntity.setDeparture(Date.from(departureAfterMinus));
        busScheduleEntity.setDropOffPoint(busScheduleInputRequestDTO.getDropOffPoint());
        busScheduleEntity.setDropOffTime(Date.from(dropOffTimeAfterMinus));
        busScheduleEntity.setPickUpTime(Date.from(pickUpTimeAfterMinus));
        busScheduleEntity.setPickUpPoint(busScheduleInputRequestDTO.getPickUpPoint());
        busScheduleEntity.setCreatedAt(Date.from(Instant.now()));
        busScheduleEntity.setDeleted(0);
        busScheduleEntity.setFare(busScheduleInputRequestDTO.getFare());
        busScheduleEntity.setEmptySeatLeft(busScheduleInputRequestDTO.getEmptySeatLeft());
        busScheduleEntity = busScheduleRepository.save(busScheduleEntity);
        BusScheduleDTO busScheduleDTO = new BusScheduleDTO();
        busScheduleDTO.setArrival(busScheduleEntity.getArrival().toString());
        busScheduleDTO.setDeparture(busScheduleEntity.getDeparture().toString());
        busScheduleDTO.setScheduleId(busScheduleEntity.getScheduleId());
        busScheduleDTO.setRouteId(busScheduleEntity.getRouteId());
        busScheduleDTO.setFare(busScheduleEntity.getFare());
        busScheduleDTO.setDropOffPoint(busScheduleEntity.getDropOffPoint());
        busScheduleDTO.setDropOffTime(busScheduleEntity.getDropOffTime().toString());
        busScheduleDTO.setPickUpPoint(busScheduleEntity.getPickUpPoint());
        busScheduleDTO.setPickUpTime(busScheduleEntity.getPickUpTime().toString());
        busScheduleDTO.setEmptySeatLeft(busScheduleEntity.getEmptySeatLeft());
        busScheduleDTO.setDeleted(busScheduleEntity.getDeleted());
        busScheduleDTO.setUpdatedAt(busScheduleEntity.getUpdatedAt()==null?null:
                busScheduleEntity.getUpdatedAt().toString());
        busScheduleDTO.setCreatedAt(busScheduleEntity.getCreatedAt().toString());
        return busScheduleDTO;
    }

    @Override
    public BusScheduleDTO updateBusSchedule(Long id, BusScheduleInputRequestDTO busScheduleInputRequestDTO) throws BusScheduleException {
        Optional<BusSchedule> busScheduleOptional = busScheduleRepository.findById(id);
        if (busScheduleOptional.isEmpty()) {
            throw new BusScheduleException("Khong tim thay lich trinh");
        }
        BusSchedule busScheduleEntity = busScheduleOptional.get();
        busScheduleEntity.setRouteId(busScheduleInputRequestDTO.getRouteId());
        busScheduleEntity.setArrival(busScheduleInputRequestDTO.getArrival());
        busScheduleEntity.setDeparture(busScheduleInputRequestDTO.getDeparture());
        busScheduleEntity.setFare(busScheduleInputRequestDTO.getFare());
        busScheduleEntity.setDropOffPoint(busScheduleInputRequestDTO.getDropOffPoint());
        busScheduleEntity.setDropOffTime(busScheduleInputRequestDTO.getDropOffTime());
        busScheduleEntity.setPickUpPoint(busScheduleInputRequestDTO.getPickUpPoint());
        busScheduleEntity.setPickUpTime(busScheduleInputRequestDTO.getPickUpTime());
        busScheduleEntity.setEmptySeatLeft(busScheduleInputRequestDTO.getEmptySeatLeft());
        busScheduleEntity.setUpdatedAt(Date.from(Instant.now()));
        busScheduleEntity = busScheduleRepository.save(busScheduleEntity);
        BusScheduleDTO busScheduleDTO = new BusScheduleDTO();
        busScheduleDTO.setScheduleId(busScheduleEntity.getScheduleId());
        busScheduleDTO.setRouteId(busScheduleEntity.getRouteId());
        busScheduleDTO.setArrival(busScheduleEntity.getArrival().toString());
        busScheduleDTO.setDeparture(busScheduleEntity.getDeparture().toString());
        busScheduleDTO.setFare(busScheduleEntity.getFare());
        busScheduleDTO.setDropOffPoint(busScheduleEntity.getDropOffPoint());
        busScheduleDTO.setDropOffTime(busScheduleEntity.getDropOffTime().toString());
        busScheduleDTO.setPickUpPoint(busScheduleEntity.getPickUpPoint());
        busScheduleDTO.setPickUpTime(busScheduleEntity.getPickUpTime().toString());
        busScheduleDTO.setEmptySeatLeft(busScheduleEntity.getEmptySeatLeft());
        busScheduleDTO.setDeleted(busScheduleEntity.getDeleted());
        busScheduleDTO.setUpdatedAt(busScheduleEntity.getUpdatedAt()==null?null
                :busScheduleEntity.getUpdatedAt().toString());
        busScheduleDTO.setCreatedAt(busScheduleEntity.getCreatedAt().toString());
        return busScheduleDTO;
    }

    @Override
    public BusScheduleDTO getBusScheduleDetail(Long id) throws BusScheduleException {
        Optional<BusSchedule> busScheduleOptional = busScheduleRepository.findById(id);
        if(busScheduleOptional.isEmpty()){
            throw new BusScheduleException("Khong tim thay lich trinh");
        }
        BusSchedule busScheduleDTOEntity = busScheduleOptional.get();
        BusScheduleDTO busScheduleDTO = new BusScheduleDTO();
        busScheduleDTO.setArrival(busScheduleDTOEntity.getArrival().toString());
        busScheduleDTO.setDeparture(busScheduleDTOEntity.getDeparture().toString());
        busScheduleDTO.setScheduleId(busScheduleDTOEntity.getScheduleId());
        busScheduleDTO.setRouteId(busScheduleDTOEntity.getRouteId());
        busScheduleDTO.setFare(busScheduleDTOEntity.getFare());
        busScheduleDTO.setDropOffPoint(busScheduleDTOEntity.getDropOffPoint());
        busScheduleDTO.setDropOffTime(busScheduleDTOEntity.getDropOffTime().toString());
        busScheduleDTO.setPickUpPoint(busScheduleDTOEntity.getPickUpPoint());
        busScheduleDTO.setPickUpTime(busScheduleDTOEntity.getPickUpTime().toString());
        busScheduleDTO.setEmptySeatLeft(busScheduleDTOEntity.getEmptySeatLeft());
        busScheduleDTO.setCreatedAt(busScheduleDTOEntity.getCreatedAt().toString());
        busScheduleDTO.setUpdatedAt(busScheduleDTOEntity.getUpdatedAt()==null?null:
                busScheduleDTOEntity.getUpdatedAt().toString());
        busScheduleDTO.setDeleted(busScheduleDTOEntity.getDeleted());
        return busScheduleDTO;
    }



    @Override
    public List<BusScheduleDTO> getAllBusSchedule() {
        List<BusSchedule> busScheduleList = busScheduleRepository.getAll();
        return busScheduleList.stream().map(x ->{
            BusScheduleDTO busScheduleDTO = new BusScheduleDTO();
            busScheduleDTO.setScheduleId(x.getScheduleId());
            busScheduleDTO.setRouteId(x.getRouteId());
            busScheduleDTO.setFare(x.getFare());
            busScheduleDTO.setDeparture(x.getDeparture()==null?null:
                    x.getDeparture().toString());
            busScheduleDTO.setArrival(x.getArrival()==null?null:
                    x.getArrival().toString());
            busScheduleDTO.setEmptySeatLeft(x.getEmptySeatLeft());
            busScheduleDTO.setPickUpTime(x.getPickUpTime()==null?null:
                    x.getPickUpTime().toString());
            busScheduleDTO.setPickUpPoint(x.getPickUpPoint());
            busScheduleDTO.setDropOffTime(x.getDropOffPoint()==null?null:
                    x.getDropOffTime().toString());
            busScheduleDTO.setDropOffPoint(x.getDropOffPoint());
            busScheduleDTO.setDeleted(x.getDeleted());
            busScheduleDTO.setCreatedAt(x.getCreatedAt()==null?null:
                    x.getCreatedAt().toString());
            busScheduleDTO.setUpdatedAt(x.getUpdatedAt()==null?null:x.getUpdatedAt().toString());
            return busScheduleDTO;
        }).toList();

    }



    @Override
    public void delete(Long id) throws BusScheduleException {
        Optional<BusSchedule> busScheduleOptional = busScheduleRepository.findById(id);
        if(busScheduleOptional.isEmpty()){
            throw new BusScheduleException("Khong tim thay lich trinh");
        }
        BusSchedule busScheduleDTOEntity = busScheduleOptional.get();
        busScheduleDTOEntity.setDeleted(1);
        busScheduleRepository.save(busScheduleDTOEntity);
    }
}
