package com.example.webclient.service.backoffice;

import com.example.webclient.dto.RouteDTO;
import com.example.webclient.dto.RouteInputRequestDTO;
import com.example.webclient.exception.RouteException;
import com.example.webclient.model.Route;

import java.util.List;

public interface RouteService {
    RouteDTO addRoute(RouteInputRequestDTO routeInputRequestDTO) throws RouteException;

    RouteDTO updateRoute(Long id, RouteInputRequestDTO routeDTO) throws RouteException;

    RouteDTO getRouteDetail(Long id) throws RouteException;

    List<RouteDTO> getAllRoute();

    void delete(Long id) throws RouteException;
}
