package com.example.webclient.service.backoffice.Impl;

import com.example.webclient.dto.RouteDTO;
import com.example.webclient.dto.RouteInputRequestDTO;
import com.example.webclient.exception.RouteException;
import com.example.webclient.model.Route;
import com.example.webclient.repository.RouteRepository;
import com.example.webclient.service.backoffice.RouteService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service

public class RouteServiceImpl implements RouteService {
    private final RouteRepository routeRepository;

    public RouteServiceImpl(RouteRepository routeRepository) {
        this.routeRepository = routeRepository;
    }

    @Override
    public RouteDTO addRoute(RouteInputRequestDTO routeInputRequestDTO) throws RouteException {
        if (routeInputRequestDTO.getRouteFrom() == null
                || routeInputRequestDTO.getRouteTo() == null ) {
            throw new RouteException("Vui Long nhap thong tin diem den va diem di");
        }
        Route routeEntity = new Route();
        routeEntity.setRouteFrom(routeInputRequestDTO.getRouteFrom());
        routeEntity.setRouteTo(routeInputRequestDTO.getRouteTo());
        routeEntity.setDeleted(0);
        routeEntity.setCreatedAt(Date.from(Instant.now()));
        routeEntity = routeRepository.save(routeEntity);
        RouteDTO routeDTO = new RouteDTO();
        routeDTO.setRouteId(routeEntity.getRouteId());
        routeDTO.setRouteFrom(routeEntity.getRouteFrom());
        routeDTO.setRouteTo(routeEntity.getRouteTo());
        routeDTO.setDeleted(0);
        routeDTO.setCreatedAt(Date.from(Instant.now()).toString());
        return routeDTO;
    }

    @Override
    public RouteDTO updateRoute(Long id, RouteInputRequestDTO route) throws RouteException {
        Optional<Route> routeOptional = routeRepository.findById(id);
        if (routeOptional.isEmpty()) {
            throw new RouteException("Khong tim thay tuyen");
        }
        Route routeEntity = routeOptional.get();
        routeEntity.setRouteFrom(route.getRouteFrom());
        routeEntity.setRouteTo(route.getRouteTo());
        routeEntity.setDeleted(0);
        routeEntity.setUpdatedAt(Date.from(Instant.now()));
        routeEntity = routeRepository.save(routeEntity);
        RouteDTO routeDTO = new RouteDTO();
        routeDTO.setRouteId(routeEntity.getRouteId());
        routeDTO.setRouteFrom(routeEntity.getRouteFrom());
        routeDTO.setRouteTo(routeEntity.getRouteTo());
        routeDTO.setDeleted(0);
        routeDTO.setUpdatedAt(routeEntity.getUpdatedAt().toString());
        routeDTO.setCreatedAt(routeEntity.getCreatedAt().toString());
        return routeDTO;
    }

    @Override
    public RouteDTO getRouteDetail(Long id) throws RouteException {
        Optional<Route> routeOptional = routeRepository.findByIdAndIsNotDeleted(id);
        if (routeOptional.isEmpty()) {
            throw new RouteException("Khong tim thay tuyen");
        }
        Route routeEntity = routeOptional.get();
        RouteDTO routeDTO = new RouteDTO();
        routeDTO.setRouteId(routeEntity.getRouteId());
        routeDTO.setRouteFrom(routeEntity.getRouteFrom());
        routeDTO.setRouteTo(routeEntity.getRouteTo());
        routeDTO.setDeleted(routeEntity.getDeleted());
        routeDTO.setCreatedAt(routeEntity.getCreatedAt().toString());
        routeDTO.setUpdatedAt(routeEntity.getUpdatedAt()==null?null:routeEntity.getUpdatedAt().toString());
        return routeDTO;
    }

    @Override
    public List<RouteDTO> getAllRoute() {
        List<Route> routeList = routeRepository.getAll();
        if(routeList.isEmpty()){
            return null;
        }
        return routeList.stream().map(x ->{
            RouteDTO routeDTO = new RouteDTO();
            routeDTO.setRouteId(x.getRouteId());
            routeDTO.setRouteFrom(x.getRouteFrom());
            routeDTO.setRouteTo(x.getRouteTo());
            routeDTO.setDeleted(x.getDeleted());
            routeDTO.setCreatedAt(x.getCreatedAt().toString());
            routeDTO.setUpdatedAt(x.getUpdatedAt() == null?null:x.getUpdatedAt().toString());
            return routeDTO;
        }).toList();
    }

    @Override
    public void delete(Long id) throws RouteException {
        Optional<Route> routeOptional = routeRepository.findById(id);
        if (routeOptional.isEmpty()) {
            throw new RouteException("Khong tim thay tuyen");
        }
        Route routeEntity = routeOptional.get();
        routeEntity.setDeleted(1);
        routeRepository.save(routeEntity);
    }
}
