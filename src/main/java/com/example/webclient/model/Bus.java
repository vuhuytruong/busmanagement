package com.example.webclient.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "bus")
public class Bus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BUS_ID", nullable = false)
    private Long busId;

    @Column(name = "SCHEDULE_ID", nullable = false)
    private Long scheduleId;

    @Column(name = "LICENSE_NUMBER")
    private String licenseNumber;

    @Column(name = "URL_IMAGE")
    private String urlImage;

    @Column(name = "SEAT_TOTAL")
    private Integer seatTotal;

    @Column(name = "IS_DELETED")
    private Integer deleted;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    public void setBusId(Long busId) {
        this.busId = busId;
    }

    public Long getBusId() {
        return busId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setSeatTotal(Integer seatTotal) {
        this.seatTotal = seatTotal;
    }

    public Integer getSeatTotal() {
        return seatTotal;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "busId=" + busId + '\'' +
                "scheduleId=" + scheduleId + '\'' +
                "liscenseNumber=" + licenseNumber + '\'' +
                "urlImage=" + urlImage + '\'' +
                "seatTotal=" + seatTotal + '\'' +
                "deleted=" + deleted + '\'' +
                "createdAt=" + createdAt + '\'' +
                "updatedAt=" + updatedAt + '\'' +
                '}';
    }
}
