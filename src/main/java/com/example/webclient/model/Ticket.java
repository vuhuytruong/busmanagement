package com.example.webclient.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TICKET_ID", nullable = false)
    private Long ticketId;

    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    @Column(name = "ROUTE_ID")
    private Long routeId;

    @Column(name = "SCHEDULE_ID")
    private Long scheduleId;

    @Column(name = "BUS_ID")
    private Long busId;

    @Column(name = "IS_DELETED")
    private Integer deleted;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setBusId(Long busId) {
        this.busId = busId;
    }

    public Long getBusId() {
        return busId;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "ticketId=" + ticketId + '\'' +
                "customerId=" + customerId + '\'' +
                "routeId=" + routeId + '\'' +
                "scheduleId=" + scheduleId + '\'' +
                "busId=" + busId + '\'' +
                "deleted=" + deleted + '\'' +
                "updatedAt=" + updatedAt + '\'' +
                "createdAt=" + createdAt + '\'' +
                '}';
    }
}
