package com.example.webclient.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "route")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ROUTE_ID", nullable = false)
    private Long routeId;

    @Column(name = "ROUTE_FROM", nullable = false)
    private String routeFrom;

    @Column(name = "ROUTE_TO", nullable = false)
    private String routeTo;

    @Column(name = "IS_DELETED")
    private Integer deleted;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteFrom(String routeFrom) {
        this.routeFrom = routeFrom;
    }

    public String getRouteFrom() {
        return routeFrom;
    }

    public void setRouteTo(String routeTo) {
        this.routeTo = routeTo;
    }

    public String getRouteTo() {
        return routeTo;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeId=" + routeId + '\'' +
                "routeFrom=" + routeFrom + '\'' +
                "routeTo=" + routeTo + '\'' +
                "deleted=" + deleted + '\'' +
                "createdAt=" + createdAt + '\'' +
                "updatedAt=" + updatedAt + '\'' +
                '}';
    }
}
