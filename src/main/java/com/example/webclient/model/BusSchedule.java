package com.example.webclient.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "bus_schedule")
public class BusSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SCHEDULE_ID", nullable = false)
    private Long scheduleId;

    @Column(name = "ROUTE_ID", nullable = false)
    private Long routeId;

    @Column(name = "DEPARTURE")
    private Date departure;

    @Column(name = "ARRIVAL")
    private Date arrival;

    @Column(name = "FARE")
    private Float fare;

    @Column(name = "PICK_UP_TIME")
    private Date pickUpTime;

    @Column(name = "PICK_UP_POINT")
    private String pickUpPoint;

    @Column(name = "DROP_OFF_TIME")
    private Date dropOffTime;

    @Column(name = "DROP_OFF_POINT")
    private String dropOffPoint;

    @Column(name = "EMPTY_SEAT_LEFT")
    private Integer emptySeatLeft;

    @Column(name = "IS_DELETED")
    private Integer deleted;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setFare(Float fare) {
        this.fare = fare;
    }

    public Float getFare() {
        return fare;
    }

    public void setPickUpTime(Date pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public Date getPickUpTime() {
        return pickUpTime;
    }

    public void setPickUpPoint(String pickUpPoint) {
        this.pickUpPoint = pickUpPoint;
    }

    public String getPickUpPoint() {
        return pickUpPoint;
    }

    public void setDropOffTime(Date dropOffTime) {
        this.dropOffTime = dropOffTime;
    }

    public Date getDropOffTime() {
        return dropOffTime;
    }

    public void setDropOffPoint(String dropOffPoint) {
        this.dropOffPoint = dropOffPoint;
    }

    public String getDropOffPoint() {
        return dropOffPoint;
    }

    public void setEmptySeatLeft(Integer emptySeatLeft) {
        this.emptySeatLeft = emptySeatLeft;
    }

    public Integer getEmptySeatLeft() {
        return emptySeatLeft;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "BusSchedule{" +
                "scheduleId=" + scheduleId + '\'' +
                "routeId=" + routeId + '\'' +
                "departure=" + departure + '\'' +
                "arrival=" + arrival + '\'' +
                "fare=" + fare + '\'' +
                "pickUpTime=" + pickUpTime + '\'' +
                "pickUpPoint=" + pickUpPoint + '\'' +
                "dropOffTime=" + dropOffTime + '\'' +
                "dropOffPoint=" + dropOffPoint + '\'' +
                "emptySeatLeft=" + emptySeatLeft + '\'' +
                "deleted=" + deleted + '\'' +
                "createdAt=" + createdAt + '\'' +
                "updatedAt=" + updatedAt + '\'' +
                '}';
    }
}
