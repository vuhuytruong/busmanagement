package com.example.webclient.enums;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public enum EntityStatus {
    ACTIVE(1, "active"),
    INACTIVE(0, "inactive");
    public int Code;
    public String Des;

    EntityStatus(int code, String description) {
        this.Code = code;
        this.Des = description;
    }

}
