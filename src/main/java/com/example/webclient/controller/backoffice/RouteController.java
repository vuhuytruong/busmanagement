package com.example.webclient.controller.backoffice;

import com.example.webclient.dto.RouteDTO;
import com.example.webclient.dto.RouteInputRequestDTO;
import com.example.webclient.exception.RouteException;
import com.example.webclient.service.backoffice.RouteService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/route")

public class RouteController {

    private final RouteService routeService;

    public RouteController(RouteService routeService) {
        this.routeService = routeService;
    }

    @PostMapping()
    public RouteDTO addRoute(RouteInputRequestDTO routeDTO) throws RouteException {
        return routeService.addRoute(routeDTO);
    }

    @PutMapping("/{id}")
    public RouteDTO updateRoute(@PathVariable("id") Long id, @RequestBody RouteInputRequestDTO routeDTO)
            throws RouteException {
        return routeService.updateRoute(id, routeDTO);
    }

    @GetMapping("/{id}")
    public RouteDTO getRouteDetail(@PathVariable("id") Long id) throws RouteException {
        return routeService.getRouteDetail(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) throws RouteException {
        routeService.delete(id);
    }

    @GetMapping()
    public List<RouteDTO> getAllRoute() {
        return routeService.getAllRoute();
    }
}
