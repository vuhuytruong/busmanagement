package com.example.webclient.controller.backoffice;

import com.example.webclient.dto.BusScheduleDTO;
import com.example.webclient.dto.BusScheduleInputRequestDTO;
import com.example.webclient.exception.BusScheduleException;
import com.example.webclient.service.backoffice.BusScheduleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bus-schedule")
public class BusScheduleController {
    private final BusScheduleService busScheduleService;

    public BusScheduleController(BusScheduleService busScheduleService) {
        this.busScheduleService = busScheduleService;
    }
    @PostMapping
    public BusScheduleDTO addBusSchedule(@RequestBody BusScheduleInputRequestDTO busScheduleDTO)
            throws BusScheduleException {
        return busScheduleService.addBusSchedule(busScheduleDTO);
    }
    @PutMapping("/{id}")
    public BusScheduleDTO updateBusSchedule(@PathVariable("id") Long id, @RequestBody BusScheduleInputRequestDTO busScheduleDTO)
            throws BusScheduleException {
        return busScheduleService.updateBusSchedule(id, busScheduleDTO);
}
    @GetMapping("/{id}")
    public BusScheduleDTO getBusScheduleDetail(@PathVariable("id") Long id) throws BusScheduleException {
        return busScheduleService.getBusScheduleDetail(id);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Long id) throws BusScheduleException {
        busScheduleService.delete(id);
    }
    @GetMapping
    public List<BusScheduleDTO> getAllBusSchedule()  {
        return busScheduleService.getAllBusSchedule();
    }
}
