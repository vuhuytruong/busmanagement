package com.example.webclient.controller.backoffice;

import com.example.webclient.dto.CustomerDTO;
import com.example.webclient.dto.RouteDTO;
import com.example.webclient.exception.CustomerException;
import com.example.webclient.service.backoffice.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")

public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{id}")
    public CustomerDTO getCustomerDetail(@PathVariable("id") Long id) throws CustomerException {
        return customerService.getCustomerDetail(id);
    }
    @GetMapping()
    public List<CustomerDTO> getAllCustomer() {
        return customerService.getAllCustomer();
}
}
