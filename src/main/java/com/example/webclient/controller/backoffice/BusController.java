package com.example.webclient.controller.backoffice;

import com.example.webclient.dto.BusDTO;
import com.example.webclient.dto.BusInputRequestDTO;
import com.example.webclient.exception.BusException;
import com.example.webclient.service.backoffice.BusService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bus")

public class BusController {

    private final BusService busService;

    public BusController(BusService busService) {
        this.busService = busService;
    }

    @GetMapping("/{id}")
    public BusDTO getDetail(@PathVariable("id") Long id) throws BusException {
        return busService.getDetail(id);
    }
    @PostMapping
    public BusDTO addBus(@RequestBody BusInputRequestDTO busDTO) throws BusException {
        return busService.addBus(busDTO);
    }
    @PutMapping("/{id}")
    public BusDTO updateBus(@PathVariable("id") Long id, @RequestBody BusInputRequestDTO busDTO) throws BusException {
        return busService.updatedBus(id, busDTO);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) throws BusException {
        busService.delete(id);
    }
    @GetMapping()
    public List<BusDTO> getAllBus() {
        return busService.getAllBus();
    }
}
