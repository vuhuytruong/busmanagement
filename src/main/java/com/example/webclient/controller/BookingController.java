package com.example.webclient.controller;

import com.example.webclient.dto.*;
import com.example.webclient.exception.BookingException;
import com.example.webclient.service.BookingService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/booking")
@AllArgsConstructor
@NoArgsConstructor
public class BookingController{

    @Autowired
    private BookingService bookingService;

    @GetMapping("/get-buses")
    public List<BusDTO> getBuses(GetBusListRequestDTO requestDTO) throws BookingException {
        return bookingService.getBusListToBook(requestDTO);
    }

    @GetMapping("/book")
    public BillDTO book(BookRequestDTO requestDTO) throws BookingException {
        return bookingService.book(requestDTO);
    }

}
