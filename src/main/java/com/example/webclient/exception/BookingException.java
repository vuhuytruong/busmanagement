package com.example.webclient.exception;

public class BookingException extends Exception{
    public BookingException(String errorMessage) {
        super(errorMessage);
    }
}
