package com.example.webclient.exception;

public class TicketException extends Exception {
    public TicketException(String errorMessage) {
        super(errorMessage);
    }
}
