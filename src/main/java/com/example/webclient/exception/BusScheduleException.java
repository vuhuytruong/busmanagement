package com.example.webclient.exception;

public class BusScheduleException extends Exception {
    public BusScheduleException(String errorMessage) {
        super(errorMessage);
    }
}
