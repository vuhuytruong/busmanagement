package com.example.webclient.exception;

public class CustomerException extends Exception {
    public CustomerException(String errorMessage) {
        super(errorMessage);
    }
}
