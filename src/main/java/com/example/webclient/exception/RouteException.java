package com.example.webclient.exception;

public class RouteException extends Exception{
    public RouteException(String errorMessage) {
        super(errorMessage);
    }
}
