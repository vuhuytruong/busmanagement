package com.example.webclient.exception;

public class BusException extends Exception {
    public BusException(String errorMessage) {
        super(errorMessage);
    }
}