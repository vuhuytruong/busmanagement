package com.example.webclient.repository;

import com.example.webclient.model.Customer;
import com.example.webclient.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = """          
            SELECT c
            FROM Customer c
            WHERE c.deleted = 0
            """)
    List<Customer> getAll();
}
