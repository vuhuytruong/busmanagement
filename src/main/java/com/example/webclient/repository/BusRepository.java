package com.example.webclient.repository;

import com.example.webclient.model.Bus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusRepository extends JpaRepository<Bus, Long> {

    @Query(value = """          
            SELECT b
            FROM Bus b
            WHERE b.scheduleId IN (:scheduleId)
            AND b.deleted = 0
            """)
    List<Bus> findByScheduleId(List<Long> scheduleId);
    @Query(value = """          
            SELECT b
            FROM Bus b
            WHERE b.deleted = 0
            """)
    List<Bus> getAll();
}
