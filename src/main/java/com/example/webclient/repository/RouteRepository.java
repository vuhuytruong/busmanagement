package com.example.webclient.repository;

import com.example.webclient.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {
    @Query(value = """          
            SELECT r
            FROM Route r
            WHERE r.routeFrom = :from
            AND r.routeTo = :to
            AND r.deleted = 0
            """)
    Optional<Route> findByRouteFromAndRouteTo(String from, String to);

    @Query(value = """          
            SELECT r
            FROM Route r
            WHERE r.deleted = 0
            """)
    List<Route> getAll();

    @Query(value = """          
            SELECT r
            FROM Route r
            WHERE r.routeId = :id
            AND r.deleted= 0
            """)
    Optional<Route> findByIdAndIsNotDeleted(Long id);
}
