package com.example.webclient.repository;

import com.example.webclient.model.BusSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface BusScheduleRepository extends JpaRepository<BusSchedule, Long> {

    @Query(value = """          
            SELECT b
            FROM BusSchedule b
            WHERE :routeId = b.routeId
            AND b.departure >= :from
            AND b.departure <= :to
            AND b.deleted = 0
            """)
    List<BusSchedule> findByRouteIdAndAndDepartureBetween(Long routeId, Date from, Date to);


  Optional<BusSchedule> findById(Long id);
    @Query(value = """          
            SELECT b
            FROM BusSchedule b
            WHERE b.deleted = 0
            """)
    List<BusSchedule> getAll();

}
